if (Meteor.isServer) {

	Meteor.startup(function () {
	
	});

	// All Users
	Meteor.publish('allUsers', function(){
		return Meteor.users.find({}, {fields: {username: 1, emails: 1} } );
	});


	// Following
	Following = new Meteor.Collection('following');

	Meteor.publish('following', function(user){
		return Following.find({followed: user}, {fields: {userId: 1, follows : 1}});
	});


	// Methods
	Meteor.methods({
		follow: function(username, toFollow) {
			var follow_id = Following.insert({userId: username, follows: toFollow})
			return follow_id;
		},

		// Unfollow a user.
		unfollow: function(username, following) {
			Following.remove({userId: username, follows: following});
		},

		// Delete all follows. For everyone.
		clearFollows: function() {
			Following.remove({});
			alert('All Follows deleted, ya dickhead');
		},

		// Delete all follows. For everyone.
		clearUsers: function() {
			Meteor.users.remove({});
			alert('All Users deleted, ya dickhead');
		}
	})

}