Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: 'loading',
	waitOn: function() { return Meteor.subscribe('allUsers'); }
});

Router.map(function() {
	this.route('home', {
		path: '/'
	});

	this.route('allUsers', {
		path: '/all-users'
	});

	this.route('search', {
		path: '/search'
	});

	this.route('admin', {
		path: '/admin',
		data: function(){
			return Meteor.users.find()
		}
	});

	this.route('profile', {
		path: '/profile/:username',
		data: function() {
			templateData = { profile: Meteor.users.findOne({username: this.params.username}, {emails: 1, username: 1}) };
			return templateData.profile;
		}
	});

	this.route('following', {
		path: '/profile/:username/following',
		data: function() {
			// get User Id based on username
			profileId = {
				username : Meteor.users.findOne({
							username: this.params.username
						}, {
							fields: {
								_id: 1
							}
						})
			};

			id = profileId.username._id;
			followArray = Following.find({userId: id}, {fields: {follows: 1, _id: 0} } ).fetch();
			followArray = _.pluck(followArray, 'follows');

			// Get usernames of people followed
			usernames = {
				follows: Meteor.users.find({
							_id: {$in: followArray}
						}, {
							fields: {
								username: 1
							}
						}).fetch()
			};
			return usernames;
		}
	});

	this.route('followers', {
		path: '/profile/:username/followers',
		data: function() {
			// get User Id based on username
			profileId = {
				username : Meteor.users.findOne({
							username: this.params.username
						}, {
							fields: {
								_id: 1
							}
						})
			};

			id = profileId.username._id;
			followArray = Following.find({follows: id}, {fields: {userId: 1, _id: 0} } ).fetch();
			followArray = _.pluck(followArray, 'userId');

			// Get usernames of followers
			usernames = {
				followers: Meteor.users.find({
							_id: {$in: followArray}
						}, {
							fields: {
								username: 1
							}
						}).fetch()
			};
			return usernames;
		}
	});

	this.route('notFound', {
	  path: '*'
	});
})