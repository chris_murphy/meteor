// All Users
Meteor.subscribe('allUsers');
Template.allUsers.Users = function(){
	return Meteor.users.find({username: {$not: null}}, {sort: {createdAt: -1}} );
};

Template.allUsers.events({
	'click .follow' : function(e, b){
		e.preventDefault();

		var userToFollow = $(e.target).attr('data-follow-id');
		var user = Meteor.user()._id;
		console.log(user+" " + userToFollow);

		Meteor.call('follow', user, userToFollow);
	},

	'click #clear-data' : function(e, b) {
		e.preventDefault();

		Meteor.call('clearData');
	}
});