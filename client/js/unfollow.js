Template.following.events({
	'click .unfollow' : function(e, b){
		e.preventDefault();

		var userFollowing = $(e.target).attr('data-following-id');
		var user = Meteor.user()._id;

		Meteor.call('unfollow', user, userFollowing);
	}
});