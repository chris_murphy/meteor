// Accounts
Template.login.events({

	'click #login-button' : function(e, b) {
		e.preventDefault();
		console.log('login clicked');

		// retrieve the input field values
		var email = b.find('#login-email').value,
			password = b.find('#login-password').value;

			// Trim and validate your fields here.... 

			// If validation passes, supply the appropriate fields to the
			// Meteor.loginWithPassword() function.
			Meteor.loginWithPassword(email, password, function(err){
			if (err) {
		console.log('login error');
				// The user might not have been found, or their passwword
				// could be incorrect. Inform the user that their
				// login attempt has failed.
				$('#error-message').text(err);
			} else {
		console.log('login success');
				// The user has been logged in.
				$('#error-message').text("");
			}
		});

		return false; 
	},

	'click #register-button' : function(e, b) {
		e.preventDefault();

		var email = b.find('#login-email').value,
			username = b.find('#login-username').value,
			password = b.find('#login-password').value;

		// Trim and validate the input

		Accounts.createUser({email: email, password : password, username: username}, function(err){
			if (err) {
				// Inform the user that account creation failed
				$('#error-message').text(err);
			} else {
				// Success. Account has been created and the user
				// has logged in successfully. 
				console.log("registration successful");
				$('#error-message').text("");
			}

		});

		return false;
	},

	'click #log-out' : function(e, b) {
		e.preventDefault();

		Meteor.logout();
	}
});

// Login
userLoggedIn = function(){
	if (Meteor.user()) {
		var currentUser = Meteor.user().username;
		// Set user
		$(".login").attr('data-status', "is-logged-in");
		$('#logged-in').attr('href', '/profile/'+currentUser+'/').text(currentUser);
	} else {
		$(".login").attr('data-status', "is-logged-out");
		$('#logged-in').attr('href', '').text('');
	};
}

Template.login.rendered = function() {
	Deps.autorun(function() {
		// Display user logged in
		userLoggedIn();
	})
}