Template.search.rendered = function(){

	function map(){
		var lat = '',
			long ='';
	}

	function loadMap(){
		var mapOptions = {
			center: new google.maps.LatLng(lat, long),
			zoom: 16,
			places: searchBox.getPlaces('pub')
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),
		mapOptions);
	};

	function searchPlaces(){

		map();

		var serachParam = 'london';


	};

	function initialize() {

		map();

		if (navigator.geolocation) {
			var timeoutVal = 10 * 1000 * 1000;
			navigator.geolocation.getCurrentPosition(
				displayPosition, 
				displayError,
				{ enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
			);
		} else {
			alert("Geolocation is not supported by this browser.\nGet a better browser");
		}

		function displayPosition(position) {
			lat = position.coords.latitude;
			long = position.coords.longitude;
			loadMap();
		};

		function displayError(error) {
			var errors = { 
				1: 'Permission denied',
				2: 'Position unavailable',
				3: 'Request timeout'
			};
			alert("Error: " + errors[error.code]);
		};

		function loadMap(){
			var mapOptions = {
				center: new google.maps.LatLng(lat, long),
				zoom: 16
			};
			var map = new google.maps.Map(document.getElementById("map-canvas"),
			mapOptions);
		};
	};
	initialize();
}