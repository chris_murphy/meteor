Template.admin.events({

	'click #clear-data' : function(e, b) {
		e.preventDefault();

		Meteor.call('clearFollows');
	},
	
	'click #clear-users' : function(e, b) {
		e.preventDefault();

		Meteor.call('clearUsers');
	}
});